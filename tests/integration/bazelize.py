# Pylint doesn't play well with fixtures and dependency injection from pytest
# pylint: disable=redefined-outer-name

import os
import pytest

from buildstream._testing.runcli import (  # pylint: disable=unused-import
    cli_integration as cli,
)
from buildstream._testing.integration import (  # pylint: disable=unused-import
    integration_cache,
)
from buildstream._testing.integration import assert_contains
from buildstream._testing._utils.site import HAVE_SANDBOX

pytestmark = pytest.mark.integration

DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "project")


@pytest.mark.datafiles(DATA_DIR)
@pytest.mark.skipif(
    not HAVE_SANDBOX, reason="Only available with a functioning sandbox"
)
@pytest.mark.parametrize("target", ["cc_library", "py_library"])
def test_generated_targets(cli, datafiles, target):
    checkout = datafiles / "checkout"
    element_name = "bazelize/{}.bst".format(target)
    expected_file = datafiles / "expected" / "BUILD.bazel.{}".format(target)

    # try to build
    result = cli.run(project=datafiles, args=["build", element_name])
    result.assert_success()

    # try to checkout
    result = cli.run(
        project=datafiles,
        args=["artifact", "checkout", "--directory", checkout, element_name],
    )
    result.assert_success()

    # check for existence of the build file
    assert_contains(str(checkout), ["/WORKSPACE", "/BUILD.bazel"])

    with open(expected_file, encoding="utf-8") as f:
        expected = f.read()

    with open(checkout / "BUILD.bazel", encoding="utf-8") as f:
        artifact = f.read()

    assert artifact == expected
