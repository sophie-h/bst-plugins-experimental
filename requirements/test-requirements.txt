pytest-env
# Provide option to run tests in parallel, less reliable
pytest-xdist
pytest >= 6.0.1
pytest-datafiles >= 2.0, < 3.0
pylint==2.16.3
pycodestyle
pyftpdlib

